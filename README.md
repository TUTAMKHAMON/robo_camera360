# Scripts
Some utility scripts can be found in the `scripts` folder. These scripts help the user configure the Raspberry Pi network and access his IP addresses without the need to have a screen and keyboard attached to the Pi.

To enable these scripts to run at startup, edit the *crontab* by calling `crontab -e` and insert the following at the bottom:
```
@reboot sleep 10 ; sudo bash /home/pi/scripts/get_wifi_from_usb.sh > /home/pi/scripts/get_wifi_from_usb.log
```
```
@reboot sleep 20 ; cd /home/pi/scripts && bash /home/pi/scripts/broadcast_ip.sh > /home/pi/scripts/broadcast_ip.log
```

**Notes:**
* *Please note that these lines assume the scripts are placed in the `/home/pi/scripts` directory.*
* *These scripts must be run using `/bin/bash` and not `/bin/sh`!*
* *These scripts were created assuming that any USB drive will be auto-mounted into /media/pi/\*. There is an option for the script to mount itself the drives, check script comments.*


## Broadcast IPs

The script `broadcast_ip.sh` broadcasts the Raspberry Pi IP addresses (both `eth0` and `wlan0`) to all USB drive connected (in a `ips.txt` file) and to a git repository defined in the script header. 

The variable `GIT_REPO` should be edited to match the desired git repository (example: `GIT_REPO="git@github.com:username/repository.git"`). 

The Raspberry Pi needs to have writing permissions to the repository, which means it is simpler to setup repository access via SSH and add the keys (tutorial on how to create the SSH keys [here](https://git-scm.com/book/en/v2/Git-on-the-Server-Generating-Your-SSH-Public-Key)).

The script will commit the IP addresses to the repository, in a branch with the name set in the `RPI_ID` variable. This ID can be overridden by having a id.txt file in the root of any USB drive connected. If the script can find this file, it will use the first line as an ID to name the branch and commit to it.

*Please note the ID should not have any spaces or other characters not allowed in branch names. Doing this will prevent the script from finishing correctly*

*This script saves the `id.txt` in the same directory as the script. So if there is no USB drive or it does not have an `id.txt` file, the script will use the previous one. This is allow the script to work without the need to have an USB drive connected.

### ID File Information
|Line|Content|Example|
|--|--|--|
|#1|ID|ROBO_Pi_AlphaBot1|

### Variables Description
| Variable Name|Description|Default Value|
| ------------- |---------------|------|
|`GIT_REPO`|The git repository to commit the  IPs. This variable should be changed and the Raspberry Pi should have writing permissions to the remote repository.|git@github.com:username/repository.git|
|`RPI_ID`|The default ID to name the branch where it will be committing. This name is overridden by any ID that is read from any USB drive.|Sample_ID|
|`USB_IPS_FILE`|The file name to write the IPs on the USB drives.|ips.txt|
|`USB_ID_FILE`|The file name to read the ID. Overrides the `RPI_ID` value. Please note this value is saved for future use!|id.txt
|`GIT_FOLDER`|The folder where the script will clone the repository into.|git-rpi|
|`GIT_IPS_FILE`|The file in the git repository to write the IPs to. The output is written in markdown language.|README.md
|`REMOTE`|The repository identification.|origin|
|`message`|The commit message (this is also used in the committed file).|IP's updated on: $(date)


## Get WIFI From USB
The script `get_wifi_from_usb.sh` will search through all connected USB drives for two files: `wifi.txt` and `peap.txt` and read the network credentials from them. These credentials are saved in the `/etc/wpa_supplicant/wpa_supplicant.conf` file, so any network created is saved for future use. This is allow the Raspberry Pi to connect to previous known networks without the need to have an USB drive connected.

The `wifi.txt` file should contain the network information about a WPA network. The first line should be the SSID and the second line the network password.

The `peap.txt` file should contain the network information about a PEAP network. The first line should be the SSID, the seconf line the username and the third line the password.

### WIFI File Information (WPA Networks)
|Line|Content|Example|
|--|--|--|
|#1|SSID|Home Network|
|#2|Password|SuperSecretPassphrase!|

### PEAP File Information (PEAP Networks)
|Line|Content|Example|
|--|--|--|
|#1|SSID|eduroam|
|#2|Username|name@institution.domain.com
|#3|Password|SuperSecretPassphrase!|

### Variables Description
|Variable Name|Description|Default Value|
| ------------- |---------------|------|
|WIFI_FILE|The name of the file containing the information about a WPA network.|wifi.txt|
|PEAP_FILE|The name of the file containing the information about a PEAP network.|peap-txt|