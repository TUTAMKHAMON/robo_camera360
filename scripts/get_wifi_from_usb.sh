#!/bin/bash

WIFI_FILE="wifi.txt"
PEAP_FILE="peap.txt"


wpa_cli -i wlan0 reconfigure
#Disabled to allow auto-mount
#sudo rm -rf /media/usb
#sudo mkdir -p /media/usb
#devices=$(lsblk -p -S -o  NAME,TRAN  | grep usb | grep /dev | awk '{ print $1; }')
#for device in $devices; do
    #printf "\nMounting $device on /media/usb...\n"
    #sudo mount $device /media/usb
    #printf "\nMounting ${device}1 on /media/usb...\n"
    #sudo mount "${device}1" /media/usb
    #cd /media/usb

for d in /media/pi/*; do
    cd $d
    printf "\nReading network file\n"

    if [ -f "$PEAP_FILE" ]; then
        printf "\nFound PEAP file\n"
        
        ssid=$(awk 'NR==1 {print; exit}' $PEAP_FILE)
        user=$(awk 'NR==2 {print; exit}' $PEAP_FILE)
        pw=$(awk 'NR==3 {print; exit}' $PEAP_FILE)
        #md5_stdin=$(echo -n "$pw" | iconv -t utf16le | openssl md4) Eduroam authentication does not support MD5 hashed passwords
        #md5=${md5_stdin:9}

        if grep -q "$ssid" /etc/wpa_supplicant/wpa_supplicant.conf; then
            id=$(wpa_cli -i wlan0 list_networks | grep "$ssid" | awk '{print $1;}')
            echo "Removing network $ssid: $(wpa_cli -i wlan0 remove_network $id)"
        fi
        
        echo "Adding network: $ssid"
        network_id=$(wpa_cli -i wlan0 add_network)

        echo "Setting SSID: $(wpa_cli -i wlan0 set_network $network_id ssid \"$ssid\")"
        echo "Setting proto: $(wpa_cli -i wlan0 set_network $network_id proto RSN)"
        echo "Setting key_mgmt: $(wpa_cli -i wlan0 set_network $network_id key_mgmt WPA-EAP)"
        echo "Setting pairwise: $(wpa_cli -i wlan0 set_network $network_id pairwise CCMP)"
        echo "Setting auth_alg: $(wpa_cli -i wlan0 set_network $network_id auth_alg OPEN)"
        echo "Setting eap: $(wpa_cli -i wlan0 set_network $network_id eap PEAP)"
        echo "Setting identity: $(wpa_cli -i wlan0 set_network $network_id identity \"$user\")"
        echo "Setting password: $(wpa_cli -i wlan0 set_network $network_id password \"$pw\")"
        echo "Setting phase1: $(wpa_cli -i wlan0 set_network $network_id phase1 \"peaplabel=0\")"
        echo "Setting phase2: $(wpa_cli -i wlan0 set_network $network_id phase2 \"auth=MSCHAPV2\")"
        echo "Enabling network: $(wpa_cli -i wlan0 enable $network_id)"
        echo "Saving configuration: $(wpa_cli -i wlan0 save_config)"
    fi

    if [ -f "$WIFI_FILE" ]; then
        printf "\nFound WIFI file\n"
        ssid=$(awk 'NR==1 {print; exit}' $WIFI_FILE)
        pw=$(awk 'NR==2 {print; exit}' $WIFI_FILE)

        if grep -q "$ssid" /etc/wpa_supplicant/wpa_supplicant.conf; then
            id=$(wpa_cli -i wlan0 list_networks | grep "$ssid" | awk '{print $1;}')
            echo "Removing network $ssid: $(wpa_cli -i wlan0 remove_network $id)"
        fi
        echo "Adding network: $ssid"
        network_id=$(wpa_cli -i wlan0 add_network)

        echo "Setting SSID: $(wpa_cli -i wlan0 set_network $network_id ssid \"$ssid\")"
        echo "Setting PSK: $(wpa_cli -i wlan0 set_network $network_id psk \"$pw\")"
        echo "Enabling network: $(wpa_cli -i wlan0 enable $network_id)"
        
        echo "Saving configuration: $(wpa_cli -i wlan0 save_config)"
    fi
    
    #printf "\nUnmounting /media/usb...\n"
    #cd /
    #sudo umount /media/usb
done
#sudo rm -rf /media/usb