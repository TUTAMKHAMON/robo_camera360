#!/bin/bash

#Define git repo here (Must be ssh and have rsa keys configured)
GIT_REPO="git@github.com:username/repository.git"

#Define Raspberry Pi ID (will be used as branch name)
RPI_ID="Sample_ID" #This id is read from USB (USB_ID_FILE)


USB_IPS_FILE="ips.txt"
USB_ID_FILE="id.txt"
GIT_FOLDER="git-rpi"
GIT_IPS_FILE="README.md"
REMOTE="origin"

message="IP's updated on: $(date)"

eth0=$(ip -4 addr show eth0 | grep -oP '(?<=inet\s)\d+(\.\d+){3}')
echo "Found Eth0: $eth0"
wlan0=$(ip -4 addr show wlan0 | grep -oP '(?<=inet\s)\d+(\.\d+){3}')
echo "Found Wlan0: $wlan0"

#USB
#Disabled to allow auto-mount
#sudo rm -rf /media/usb
#sudo mkdir -p /media/usb
#devices=$(lsblk -p -S -o  NAME,TRAN  | grep usb | grep /dev | awk '{ print $1; }')
#for device in $devices; do
    #printf "\nMounting $device on /media/usb...\n"
    #sudo mount $device /media/usb
    
    #printf "\nWriting IPs...\n"
    #sudo touch /media/usb/$USB_IPS_FILE
    #echo "Eth0:\n$eth0\n\nWlan0:\n$wlan0" > /media/usb/$USB_IPS_FILE

    #printf "\nUnmounting /media/usb...\n"
    #sudo umount /media/usb
#done
##sudo rm -rf /media/usb

#USB
for d in /media/pi/*; do
    printf "\nWriting IPs...\n"
    sudo touch "$d/$USB_IPS_FILE"
    printf "Eth0:\n$eth0\n\nWlan0:\n$wlan0" > "$d/$USB_IPS_FILE"

    if [ -f "$d/$USB_ID_FILE" ]; then
        id=$(awk 'NR==1 {print; exit}' "$d/$USB_ID_FILE")
        printf "\nFound ID file with ID: $id\n"
        touch $USB_ID_FILE
        printf "$id" > $USB_ID_FILE
    fi
done

#GIT
if [ -f $USB_ID_FILE ]; then
    RPI_ID=$(awk 'NR==1 {print; exit}' $USB_ID_FILE)
fi

git clone $GIT_REPO $GIT_FOLDER
cd $GIT_FOLDER

printf "\nChecking out $RPI_ID...\n"
git checkout -B $RPI_ID
printf "\nPulling...\n"
git pull -f $REMOTE $RPI_ID

echo "\Writing to file..."
touch $GIT_IPS_FILE
echo "
# $RPI_ID
\`\`\`
Eth0: $eth0
\`\`\`
\`\`\`
Wlan0: $wlan0
\`\`\`
*$message*
" > $GIT_IPS_FILE

printf "\nCommiting...\n"
git config user.email "$RPI_ID@robo.fe.up.pt"
git config user.name "$RPI_ID"
git add -A
git commit -am "$message"
printf "\nPushing to $REMOTE:$RPI_ID\n"
git push $REMOTE $RPI_ID
cd ..

printf "\nCleaning up...\n"
rm -rf "./$GIT_FOLDER"