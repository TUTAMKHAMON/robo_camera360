\documentclass[conference]{IEEEtran}
\IEEEoverridecommandlockouts
% The preceding line is only needed to identify funding in the first footnote. If that is unneeded, please comment it out.
\usepackage{cite}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{xcolor}
\usepackage{multirow}
\usepackage{amsmath}
\usepackage{gensymb}
\def\BibTeX{{\rm B\kern-.05em{\sc i\kern-.025em b}\kern-.08em
    T\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}
\begin{document}

\title{Low-budget Solution for Surround Obstacle Avoidance Using Computer Vision in Robots\\
%{\footnotesize \textsuperscript{*}Using computer vision and a spherical mirrored light bulb
%should not be used}
%\thanks{Identify applicable funding agency here. If none, delete this.}
}

\author{
\IEEEauthorblockN{Filipe Coelho}
\IEEEauthorblockA{\textit{Department of Informatics Engineering} \\
\textit{Faculty of Engineering of the University of Porto}\\
Porto, Portugal \\
up201500072@fe.up.pt}
\and
\IEEEauthorblockN{Jo\~ao Ferreira}
\IEEEauthorblockA{\textit{Department of Informatics Engineering} \\
\textit{Faculty of Engineering of the University of Porto}\\
Porto, Portugal \\
201405163@fe.up.pt}
}

\maketitle

\begin{abstract}
The main purpose of this project is to create a new implementation of obstacle detection and avoidance systems using cheaper and simpler alternatives to traditional methods, such as sonars. This implementation uses a camera pointed at a semi-spherical mirror to view the robot surroundings, and the Open Computer Vision (OpenCV) library to process the frames and detect obstacles. A detailed prototype is presented from the construction of the alternative body structure, calculations for the mirror placement distance as well as the obstacle recognition process. 

In this paper is also described utilities the team created and implemented to facilitate the remote access to the robot. Those utilities include automatic network configuration, Internet Protocol (IP) address broadcasting and remote access servers.

The innovation of this work resides in the use of a camera and a mirror implemented in the robot itself to analyse its surroundings instead of the usual approach of three sonars on the front and one in the back. This method allows for a large coverage angle, being limited only by the mirror supports that cut the camera vision.

Finally, the experiments were conducted on an AlphaBot2 kit using a Raspberry Pi 3 Model B+ and a RPi Camera B Rev 2.0, with obstacles having a clear distinct color from the background.
\end{abstract}

%\begin{IEEEkeywords}
%navigation, 
%\end{IEEEkeywords}

\section{Introduction}
Navigation in autonomous robots is not a new subject. Since robots became mobile, navigation has been a main filed of study \cite{Gomez2016}. However, hardware is very different from the early times, not only sensors became faster and more precise, batteries grown, materials became more accessible, but also computing units are made smaller, more efficient and much more faster \cite{Michael2010}. These innovations provide a greater autonomy for robots. They can endure longer run times without the need to recharge, can detect obstacles easier, predict and calculate new paths faster and solve more complicated problems. 

Using computer vision to create autonomy in robots has been seen implemented with success in a lot of products, most knowingly in autonomous cars. Despite that, many computer vision algorithms used are still too heavy to implement in lower end hardware like the Raspberry Pi 3 and have a real-time frame processing, specially considering the amount of variables systems like the ones implemented in autonomous cars are subjected. But if we have a controlled environment, where the robot navigates through a semi-static background and the obstacles are marked with very distinct colors, simpler algorithms could be used to detect obstacles.

In this project, an AlphaBot2 kit was used as a base for the autonomous robot. As the computing unit, a Raspberry Pi 3 Model B+ was used, with a Pi Camera installed. The team design a support for the camera on top of the unit, and the supports for a mirror, where a light bulb that has half spherical mirror surface ``figure \ref{fig:lightbulb}'' was hanged upside down.

\begin{figure}[htbp]
\centerline{\includegraphics[width=0.25\textwidth]{images/lightbulb.jpg}}
\caption{Light bulb with half spherical mirror surface.}
\label{fig:lightbulb}
\end{figure}

\section{Related Work}
Newer computing units provide a lot more computing power in a small form factor, allowing for tasks such as object detection based on computer vision to be possible. Object detection is one of most important features in intelligent robotics \cite{Chen2018}, and there is a great research and scientific documentation on this subject. Several types of vehicles are running computer vision algorithms with real-time performance \cite{Chen2018} to aid navigation and provide a greater deal of security \cite{Kho2014}, such as unmanned aerial vehicles (UAVs)\cite{Darma2013} \cite{Lim2015} \cite{Colomina2014} \cite{Ergezer2013} or automated guided vehicles (AGVs) \cite{Fontanelli2015}. 

Obstacle detection is a subset of object detection, and the ability to detect objects allows robots to perform more elaborated tasks than only obstacle avoidance, like object/vehicle tracking \cite{Kho2014}, human detection and action recognition \cite{Hoshino2018} or object searching and identification. These tasks became more intuitive if the sensor(s) used to detect the objects are cameras. This allows to use computer vision algorithms to accomplish the given challenges. Some of those implementations involve feature matching, scale invariant algorithms like SIFT \cite{Lowe2004} or SURF \cite{Bay2006}, others use machine learning techniques like Convolutional Neural Networks (CNNs) \cite{Hoshino2018}, but since this project is only about detecting obstacles that have a distinct color from a semi-uniform background, a more lightweight approach was taken, based only on image color processing.

\section{Proposal}
Our proposal to solve this problem was divided in several steps:
\begin{itemize}
    \item Initial Configuration
    \begin{itemize}
        \item Install and configure Raspberry Pi with Raspbian Operating System;
        \item Install and configure Virtual Network Computing (VNC) remote access;
        \item Install Raspberry Pi camera module;
        \item Install Raspberry Pi as an AlphaBot2 main board;
        \item Build and install OpenCV library
    \end{itemize}
    \item Utilities
    \begin{itemize}
        \item Implement solution to connect to new networks without remote access;
        \item Implement solution to broadcast IP addresses
    \end{itemize}
    \item Support Structures
    \begin{itemize}
        \item Design and build a camera mount;
        \item Design and build a mirror mount;
    \end{itemize}
    \item Solution
    \begin{itemize}
        \item Implement ground detection;
        \item Implement object detection;
        \item Abstract camera information as sonars;
        \item Detect obstacles and avoid them.
    \end{itemize}
\end{itemize}

This allowed us to divide the problem and start working on smaller tasks towards the final goal. It also allowed us to work in parallel in tasks that were not dependent on previous ones to function.

\subsection{Initial Configuration}
After acquiring all the hardware required for this project, we started by formatting and flashing a MicroSD card with the Raspbian Operating System. After the first boot, we installed a VNC server and configured it to allow remote access. This proved to be really valuable in the future, because every access to the Raspberry Pi from now on could be made from our own computers instead of being directly connected to the Raspberry Pi. While this was being made, the camera was also connected to the Raspberry Pi to ensure it was working. Once everything was running, it was time to connect the raspberry pi to the AlphaBot2 kit. This was pretty straightforward, since the AlphaBot2 is made to be used with the Raspberry Pi (although the AlphaBot2 has a buzzer that goes on every time it is turned on). So we created a script to disable the buzzer at boot and after a few tests to ensure the Pi could control the AlphaBot2, it was time to build and install the OpenCV library from source. Although it was pretty straightforward, this process takes a long time, because building a large software like the OpenCV library from source is a very demanding task, which a low end computer board like the Raspberry Pi struggles with.

With that accomplished, we created a few scripts to evaluate the connection between the RPi Camera and the OpenCV. But there was a problem however: The Raspberry Pi was connected to the network via Ethernet. But to allow for autonomous movement of the AlphaBot2, we needed to connect it through a wireless network. Adding this network was not a problem, since we already had remote access to the Raspberry Pi. But after adding the wireless network we would not know the new IP address given to the Raspberry Pi. Also, every time we restarted the AlphaBot2, or used it outside the range of our wireless network, a new problem raised: How could we tell the Raspberry Pi a new wireless network credentials if we could not access to it? So we developed two utility scripts to help us overcome these issues.

\subsection{Utilities}
To address the problem of connecting to a new wireless network without having to physically access the Raspberry Pi, we designed a script that looks for any Universal Serial Bus (USB) flash drive connected and searches at the root of them for a text file with a specific name, and inside that text file the first line would correspond to the network service set identifier (SSID) and the second line would correspond to the password. 

If this file was found and the SSID and password were retreived successfully from it, the script would then analyse the existing ``wpa\_spliucant.conf'' file and find out if we were updating the credentials of an existing network, or trying to setup a new one. Then it resorts to the ``wpa\_cli'' utility to configure the network and finally it restarts the networking system on the Raspberry Pi, for it to read the new network credentials.

With this problem solved, we have to had a way of knowing the new IP address of the Raspberry Pi. For that, we would want to be able to receive the new addresses on our computer without connecting to the Raspberry Pi. For that we needed some external service where the Raspberry Pi could broadcast its IP addresses and we could see the listing using a web browser. So we created a Google App Script published as a web app that receives the information via URL encoded parameters and sends an email to a preconfigured account with the new addresses. While this worked well, this solution wouldn't be easily portable to other instances and would quite easily clutter the mail inbox with emails that rapidally became obsolete. 

So we thought tagging each instance of the AlphaBot2 with an unique identifier and creating a dedicated GitHub repository, where each instance of the AlphaBot2 has its own branch, represented by the identifier. So, the script starts by checking out a branch that corresponds to the identifier provided (creating it if the branch does not exist), then writes to a file the IP addresses and finally commits and pushes the file to the GitHub repository. To allow committing this file without the need to provide credentials, an secure shell (SSH) key was generated and added to repository. This solution was simpler to implement and much more portable, since the script can live in the master branch of the repository, and for each instance of the AlphaBot it is only necessary to add a new SSH key to the repository, clone it and change a variable that represents the unique identifier of the AlphaBot. After that, on the next reboot, a new branch is created and pushed to the GitHub repository. This had also the advantage of all of the AlphaBots using this system were listed in a single location, always up to date.

This solution worked quite well, except in one particular case: when the network to connect does not have an internet connection. This was a very particular problem, since it did not happened much often. And for that case, we thought of adapting the previous script to also write the addresses on a text file inside a USB flash drive that would be connected to the Raspberry Pi. If the USB was found, the text file was saved, if not, this step was simply ignored.

With these utility scripts implemented, we could bring our system anywhere and work on it, knowing we would always be able to remote access it, even if the network was not previously configured. We also added a quick response (QR) code and a near-field communication (NFC) tag that pointed right at the branch of our specific AlphaBot.

\subsection{Support Structures}
Another very important task that was being worked on in parallel with the utility scripts was the support structures for the RPi Camera and the light bulb mirror. So we started to make a replica of the AlphaBot top board in blender. This replica would contain everything important from the main one, such as the dimensions, cutouts and screw locations and dimensions ``figure \ref{fig:alphabot_top_board}''.

\begin{figure}[htbp]
\centerline{\includegraphics[width=0.25\textwidth]{images/alphabot_top_board.PNG}}
\caption{AlphaBot2 top board 3D model replicated in Blender.}
\label{fig:alphabot_top_board}
\end{figure}

With 3D model of the base board, we could design a camera mount centered in the board that used the already made holes. Since these models were going to be 3D printed, we divided the mount in two pieces that would later be screwed in the AlphaBot board and hold the camera in place ``figure \ref{fig:camera_mount}''. One of the pieces has two clips that when the camera slides in they hold it in place.

\begin{figure}[htbp]
\centerline{\includegraphics[width=0.45\textwidth]{images/camera_mount_two.png}}
\caption{The two pieces of the camera mount on the left and their installation on the board on the right.}
\label{fig:camera_mount}
\end{figure}

A bigger task was to design the mirror mount. The mirror must stay centered with the base, elevated from the camera, and sturdy so it does not wobble with the AlphaBot movement. After several iterations, we end up with an armature around the board AlphaBot board and a four-legged design bringing the mirror up ``figure \ref{fig:all_mount} left''. The bottom part of the legs is made in a screw shape and this allows for fine adjustments of the distance between the mirror surface and the camera, specially considering that screwing the legs in 5 spins already gives a pretty sturdy structure ``see table \ref{tab:mirror_support_properties}''.

\begin{figure}[htbp]
\centerline{\includegraphics[width=0.485\textwidth]{images/all_mounted.png}}
\caption{Mirror mount on the left and the whole support structures mounted on the center (wire-frame mode) and right.}
\label{fig:all_mount}
\end{figure}

\begin{table}[htbp]
\caption{Mirror support structure properties}
\begin{center}
\begin{tabular}{|p{35mm}|c|c|}
\hline
\textbf{Upper Leg Height}                                 & \multicolumn{2}{c|}{205\,mm}         \\ \hline
\textbf{Lower Leg Height}                                 & \multicolumn{2}{c|}{84\,mm}          \\ \hline
\textbf{Upper Leg Threads}                                & \multicolumn{2}{c|}{75}              \\ \hline
\textbf{Lower Leg Threads}                                & \multicolumn{2}{c|}{78}              \\ \hline
\textbf{Thread Pitch}                                     & \multicolumn{2}{c|}{1\,mm}           \\ \hline
\multirow{2}{*}{\textbf{Total Leg Height Variance}}       & \textbf{Min}      & \textbf{Max}     \\ \cline{2-3}
                                                          & 214\,mm           & 284\,mm          \\ \hline
\textbf{Distance from Lamp Socket to Light Bulb base}     & \multicolumn{2}{c|}{127\,mm}         \\ \hline
\textbf{Distance from AlphaBot Board to Camera Lens}      & \multicolumn{2}{c|}{27\,mm}          \\ \hline
\textbf{Distance between Camera}                          & \textbf{Min}      & \textbf{Max}     \\ \cline{2-3}
\textbf{Lens and Light Bulb base}                         & 60\,mm              & 130\,mm            \\ \hline
\end{tabular}
\label{tab:mirror_support_properties}
\end{center}
\end{table}

The reason of having two pairs of two legs close to each other instead of only three legs was to unclutter the front and back as much as possible. For three legs to provide a sturdy mount, forced at least one of them to obstruct the view to the front or back of the AlphaBot. With this setup, we shifted the legs to the sides, opening the vision at the front and at the back. Finally, the lamp socket was a simple E27 lamp socket with 4 slots for the legs to slide in. The final assembly can be seen in the figure \ref{fig:all_mount} in the middle and in the right.

\subsection{Solution}
The solution consists of the following steps:
\begin{itemize}
    \item Place the mirror surface at an ideal height;
    \item Capture a frame from the mirror;
    \item Use a polar coordinate system to transform the circular reflection captured into linear frame;
    \item Mask out the part of the linear frame that corresponds to the robot;
    \item Detect when there is an object in the linear frame, and correspond it to an angle and a distance on the AlphaBot.
\end{itemize}

To calculate the height that the mirror surface should be, we resorted the diagram visible in the ``figure \ref{fig:geometry}''. From it we deducted the equations \ref{eq:systemP}, \ref{eq:calculateP}, \ref{eq:calculateA}, \ref{eq:theta4}  and \ref{eq:distance} to relate the light bulb radius, the height of the mirror surface, the height of the camera lens and the field of view (FOV) with the maximum distance to detect the objects.

\begin{figure}[htbp]
\centerline{\includegraphics[width=0.485\textwidth]{images/geometry.png}}
\caption{Distance relation diagram.}
\label{fig:geometry}
\end{figure}

\begin{equation}
    \resizebox{0.6\hsize}{!}{$
    \left\{\begin{matrix}
    py = cotan(\theta_1)px
    \\
    px^2 + (py-(h+r))^2 = r^2
    \end{matrix}\right.
    $}
    \label{eq:systemP}
\end{equation}

\begin{equation}
    \resizebox{0.9\hsize}{!}{$
        \left [ px = \frac{(r+h)cot(\theta_1)-A}{cot^2(\theta_1)+1}, py = \frac{(r+h)cot^2(\theta_1)-A\:  cot(\theta_1)}{cot^2(\theta_1) + 1} \right ]
    $}
    \label{eq:calculateP}
\end{equation}

\begin{equation}
        \resizebox{0.6\hsize}{!}{$
            A = \sqrt{r^2 cot^2(\theta_1) - 2hr - h^2}
        $}
    \label{eq:calculateA}
\end{equation}
\begin{equation}
        \resizebox{0.6\hsize}{!}{$
            \theta_2 = atan\left ( \frac{-r + py + h}{px} \right ) + \frac{\pi }{2}
        $}
    \label{eq:theta2}
\end{equation}


\begin{equation}
    \resizebox{0.4\hsize}{!}{$
        \theta_4 = 2\;  \theta_2 + \theta_1 - \frac{\pi}{2}
    $}
    \label{eq:theta4}
\end{equation}

\begin{equation}
    \resizebox{0.4\hsize}{!}{$
        d = px - \frac{py + H}{tan(\theta_4)}
    $}
    \label{eq:distance}
\end{equation}

\vspace{3.5mm}

To capture the frame from the mirror, we implemented a zoom function. This function would crop the frame on the GPU by the time it was took, thus preserving resolution and maintaining speed, given by:\\
\begin{equation}
    \resizebox{0.9\hsize}{!}{$
        crop = (x,y,w,h) = (0.5 - \frac{0.5}{zoom}, 0.5 - \frac{0.5}{zoom}, \frac{1}{zoom}, \frac{1}{zoom})
    $}
    \label{eq:crop}
\end{equation}

\vspace{3mm}

This function is important because the mirror surface in the light bulb is not perfectly round, which means that the center of the polar coordinate system could not be the center of the captured frame. To overcome that, we marked the ring around the camera lens with a fluorescent color and captured a frame with $zoom = 5$ and $resolution = (512; 512)px$ ``figure \ref{fig:center_detection} left'', threshold it with $hue \in [37; 42]$, $saturation \in [175; 255]$ and $value \in [150; 255]$ ``figure \ref{fig:center_detection} center'' and used a circle detection algorithm presented in OpenCV to detect the circle and its center ``figure \ref{fig:center_detection} right''.

\begin{figure}[htbp]
\centerline{\includegraphics[width=0.485\textwidth]{images/center_capture.png}}
\caption{Center of polar coordinate system detection.}
\label{fig:center_detection}
\end{figure}

Now that have the center of our polar coordinate system, we capture a new frame with $zoom = 1.1$ and $resolution = (360; 360)px$. This new frame catches the reflection of the surroundings of the robot while maintaining the same lens center point ``figure \ref{fig:transform_linear} left''. To translate the center between the two frames we use equations \ref{eq:resizeZoom} and \ref{eq:resizeRes}.\\
\begin{equation}
    \resizebox{0.9\hsize}{!}{$
        newCenter = oldCenter + resolution/2 * \left [ 1 - \frac{newZoom}{oldZoom} \right ]
    $}
    \label{eq:resizeZoom}
\end{equation}

\begin{equation}
    \resizebox{0.7\hsize}{!}{$
        newCenter = oldCenter * \frac{newResolution}{oldResolution}
    $}
    \label{eq:resizeRes}
\end{equation}

\vspace{3.5mm}

With the center of the polar coordinate system found in the second frame, we proceed to project the polar coordinates into a linear system and obtained an image with linear distances from the robot's edge ``figure \ref{fig:transform_linear} right''. 


\begin{figure}[htbp]
\centerline{\includegraphics[width=0.485\textwidth]{images/linar_system.png}}
\caption{Transforming spherical capture into linear system.}
\label{fig:transform_linear}
\end{figure}

We then detected an object by analysing the color differences between the background and then used the same equations \ref{eq:systemP}, \ref{eq:calculateP}, \ref{eq:calculateA}, \ref{eq:theta4}  and \ref{eq:distance} to relate the light bulb radius, the height of the mirror surface, the height of the camera lens and the field of view (FOV) with the distance the detected object is.

\section{Experiments and Results}
After resolving the equations \ref{eq:systemP}, \ref{eq:calculateP}, \ref{eq:calculateA}, \ref{eq:theta4}  and \ref{eq:distance} we concluded that we needed a height of 116.6mm and a vertical FOV of
33.66\degree or a height of 77.9mm and a horizontal FOV of 43.85\degree to view the whole light bulb. 

However, after setting that height, we analyzed that viewing the whole light bulb was to wasteful. We needed higher resolution, which slowed down the computing time and a lot of the reflected space was reflected towards infinity. So, we decided to put the mirror surface at a height of 100mm, with a FOV of 29.56\degree, which is close to the 33.66\degree. Please notice that since we crop the image to a square proportion, only the smaller of the FOVs is important.

To test the system, we placed the AlphaBot in a location where the background had a uniform grayish color, surrounded by objects with very distinct colors ``fig\ref{fig:test_map}''. 

\begin{figure}[htbp]
\centerline{\includegraphics[width=0.485\textwidth]{images/test_map.jpg}}
\caption{AlphaBot surrounded by obstacles with distinct colors.}
\label{fig:test_map}
\end{figure}

The results related to the object detection were quite satisfactory. The system detected successfully the obstacles in surrounding area, and correctly identified their direction and distance from the AlphaBot. The implementation of the obstacle avoidance was rather simple and was not target of too many tests. This system was only a proof of concept to demonstrate that the obstacle detection was working and it was not part of the main focus of this project. Still, it worked quite well, never colliding against any of the obstacles, serving as further proof that the object detection system was working.

\section{Conclusions and Future Work}
Implementing a 360\degree obstacle detection system on a low-budget system is a challenging task. The system clear bottleneck is the Raspberry Pi's computing power. Even so, we consider this implementation to be successful, as it only affects the detection speed and, consequently, the movement speed the robot could go. 

Creating the support structures was a fun task for the team. Trying to find out a way of connecting a new structure to the already made AlphaBot board without adding anything else to it was demanding, specially figuring out the height values we would want to position the light bulb. 

During the development of the support structures, access to 3D printers was fundamental, since they could build quickly new iterations of the designs in a sturdy material. With their help, in one week we had a design that was close to the final one, and were ready to work on the remaining tasks.

This system is not complete, though. The development team focused a lot more in obstacle detection rather than their avoidance. While it works, it is not reliable. It could also benefit from autonomous navigation systems, but they fall outside the scope of this project.

\bibliography{references}{}
\bibliographystyle{plain}

\end{document}
