from camera import Camera
import cv2
import numpy
import time
from AlphaBot2 import AlphaBot2


class StreamProcessor:

	def __init__(self, resolution=(360, 360), board_radius=130):
		self.resolution = resolution
		self.board_radius = board_radius
		self.frame_count = 0
		self.center = (resolution[0] / 2, resolution[1] / 2)
		self.mask = self.make_bot_mask()
		self.average_ground_color = (0, 0, 0)
		self.surrounding_objects = []
		self.running = True
		self.debug = False

	def find_camera_lens(self, frame):
		frame_HSV = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
		frame_threshold = cv2.inRange(frame_HSV, (37, 175, 150), (42, 255, 255))
		elliptical_kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5))
		frame_threshold = cv2.morphologyEx(frame_threshold, cv2.MORPH_CLOSE, elliptical_kernel, iterations=3)

		_, contours, _ = cv2.findContours(frame_threshold, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
		center, radius = cv2.minEnclosingCircle(contours[0])
		center = (int(center[0]), int(center[1]))
		radius = int(radius)

		### Draw the results
		if self.debug:
			cv2.imshow("Frame - zoom 5", frame)
			cv2.drawContours(frame, contours, -1, (255, 0, 0), 1)
			cv2.circle(frame, center, radius, (0, 0, 255))
			cv2.line(frame, (center[0], 0), (center[0], frame.shape[0]), (0, 0, 255))
			cv2.line(frame, (0, center[1]), (frame.shape[1], center[1]), (0, 0, 255))
			cv2.imshow("Frame center - zoom 5", frame)
			cv2.imshow("Camera detected mask", frame_threshold)
			cv2.waitKey(0)

		self.center = center
		return center

	def set_center(self, center):
		self.center = center

	def make_bot_mask(self):
		width = self.resolution[1] - self.board_radius

		mask = numpy.zeros((self.resolution[0], width, 1), numpy.uint8)
		mask[:, :] = 255

		arm_width = 15
		arm_positions = [15, 150, 192, 335]  # empirical values

		for arm_position in arm_positions:
			cv2.rectangle(mask, (0, arm_position), (width, arm_position + arm_width), 0, -1)

		return mask

	def calculate_average_ground(self, frame):
		current_avg = cv2.mean(frame, self.mask)
		c = self.frame_count

		self.average_ground_color = tuple([x/c*(c-1) + y/c for x, y in zip(self.average_ground_color, current_avg)])

	def process_frame(self, frame):
		self.frame_count += 1

		imagepol = cv2.linearPolar(frame, self.center, frame.shape[0] / 2, cv2.WARP_FILL_OUTLIERS)

		# draw normal frame with camera center
		if self.debug:
			cv2.line(frame, (self.center[0], 0), (self.center[0], frame.shape[0]), (0, 0, 255))
			cv2.line(frame, (0, self.center[1]), (frame.shape[1], self.center[1]), (0, 0, 255))
			cv2.imshow("Frame center", frame)

		# crop image to remove board
		imagepol = imagepol[0:self.resolution[0], self.board_radius:self.resolution[1]]

		# apply mask
		imagepol = cv2.bitwise_and(imagepol, imagepol, mask=self.mask)

		# remove noise
		imagepol = cv2.GaussianBlur(imagepol, (5, 5), 0)

		# tranform to HSV colorspace
		imagepol_HSV = cv2.cvtColor(imagepol, cv2.COLOR_BGR2HSV)

		# get the combination of saturation and value
		imagepol_H, imagepol_S, imagepol_V = cv2.split(imagepol_HSV)
		imagepol_SV = cv2.multiply(imagepol_S/255, imagepol_V/255)
		imagepol_SV = numpy.array([x*255 for x in imagepol_SV])
		imagepol_SV = cv2.normalize(imagepol_SV, None, 0, 255, cv2.NORM_MINMAX, cv2.CV_8U)
		imagepol_SV = cv2.medianBlur(imagepol_SV, 5)
		if self.debug:
			cv2.imshow("Imagepol_SV", imagepol_SV)


		# SIMPLE SHARP APPROACH
		# kernel = numpy.array([[-1, -1, -1], [-1, 9, -1], [-1, -1, -1]])
		# sharp = cv2.filter2D(imagepol_SV, -1, kernel)
		# cv2.imshow("Sharp", sharp)
		# _, mask_objects = cv2.threshold(sharp, 65, 255, cv2.THRESH_BINARY)
		# kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3))
		# mask_objects = cv2.morphologyEx(mask_objects, cv2.MORPH_CLOSE, kernel)
		# kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5))
		# mask_objects = cv2.morphologyEx(mask_objects, cv2.MORPH_OPEN, kernel)
		# cv2.imshow("Objects sharp", mask_objects)


		# COMPLEX FILTER APPROACH
		kernel = numpy.array([[-1, -1, -1, -1, -1],
							  [-1,  1,  1,  1, -1],
							  [-1,  1,  9,  1, -1],
							  [-1,  1,  1,  1, -1],
							  [-1, -1, -1, -1, -1]])
		complex = cv2.filter2D(imagepol_SV, -1, kernel)
		_, mask_objects = cv2.threshold(complex, 90, 255, cv2.THRESH_BINARY)
		kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5))
		mask_objects = cv2.morphologyEx(mask_objects, cv2.MORPH_OPEN, kernel)
		mask_objects = cv2.morphologyEx(mask_objects, cv2.MORPH_CLOSE, kernel)
		if self.debug:
			cv2.imshow("Test", complex)
			cv2.imshow("Objects test", mask_objects)
			imagepol_S = cv2.bitwise_and(imagepol_S, imagepol_S, mask=mask_objects)
			image_sel = cv2.cvtColor(cv2.merge((imagepol_H, imagepol_S, imagepol_V)), cv2.COLOR_HSV2BGR)
			cv2.imshow("Imgpol", image_sel)

		_, objects, _ = cv2.findContours(mask_objects, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

		self.surrounding_objects = [cv2.boundingRect(obj) for obj in objects]

		# filter noise
		self.surrounding_objects = [obj for obj in self.surrounding_objects if obj[2] * obj[3] > 50 or obj[0] > 120]

		if self.debug or True:
			[cv2.rectangle(imagepol, (obj[0], obj[1]), (obj[0] + obj[2], obj[1] + obj[3]), (0, 0, 255)) for obj in self.surrounding_objects]
			cv2.line(imagepol, (0, 90), (self.resolution[1], 90), (0, 255, 0))
			cv2.imshow("Object boxes", imagepol)

		key = cv2.waitKey(1)

		if key == 27 or key == 32 or key == 113:
			self.running = False
			return -1
		return 0


capture_resolution = (512, 512)

if __name__ == '__main__':
	sp = StreamProcessor()

	camera = Camera(resolution=capture_resolution, zoom=5)
	camera_center = sp.find_camera_lens(camera.get_frame())

	camera.set_zoom(1.1)
	camera_center = camera.transform_coordinates_zoom(camera_center, 5, 1.1)
	camera.set_resolution(sp.resolution)
	camera_center = camera.transform_coordinates_resolution(camera_center, capture_resolution)
	sp.set_center(camera_center)

	camera.subscribe_stream(lambda frame: sp.process_frame(frame))
	camera.start_stream_capture()

	Ab = AlphaBot2()
	while sp.running:
		if len(sp.surrounding_objects) > 0:
			dists = numpy.array([obj[0] for obj in sp.surrounding_objects])
			min_idx = numpy.argmin(dists)
			closest_obj = sp.surrounding_objects[min_idx]
			angle = closest_obj[1]+closest_obj[3]/2
			if angle < 80 or angle > 270:
				print("RIGHT: " + str(angle) + " | " + str(closest_obj))
				Ab.rightSpeed(17)
			elif angle > 100:
				print("LEFT: " + str(angle) + " | " + str(closest_obj))
				Ab.leftSpeed(17)
			else:
				print("====: " + str(angle) + " | " + str(closest_obj))
				Ab.stop()
			time.sleep(0.01)
