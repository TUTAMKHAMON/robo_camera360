import threading
import time
import picamera.array

class Camera:

	def __init__(self, resolution=(512, 512), zoom=1, camera_mode=2):
		self.camera = picamera.PiCamera()
		self.camera.sensor_mode = camera_mode
		self.camera.resolution = resolution
		self.camera.crop = (0.5 - 0.5 / zoom, 0.5 - 0.5 / zoom, 1 / zoom, 1 / zoom)
		self.camera.framerate = 90
		self.camera.led = False
		self.camera.start_preview()
		self.stream_subscribers = []
		self.running = False
		self.last_frame = 0
		self.debug = False
		time.sleep(1)

	def __del__(self):
		self.camera.close()

	def get_frame(self):
		with picamera.array.PiRGBArray(self.camera) as stream:
			self.camera.capture(stream, format='bgr', use_video_port=True)
			image = stream.array
			return image

	def set_sensor_mode(self, sensor_mode):
		self.camera.sensor_mode = sensor_mode

	def set_resolution(self, resolution):
		self.camera.resolution = resolution

	def set_zoom(self, zoom):
		self.camera.crop = (0.5 - 0.5 / zoom, 0.5 - 0.5 / zoom, 1 / zoom, 1 / zoom)

	def set_settings(self, sensor_mode=None, resolution=None, zoom=None):
		if sensor_mode is not None:
			self.set_sensor_mode(sensor_mode)

		if resolution is not None:
			self.set_resolution(resolution)

		if zoom is not None:
			self.set_zoom(zoom)

	def _start_stream_capture(self):
		self.running = True
		with picamera.array.PiRGBArray(self.camera) as stream:
			for frame in self.camera.capture_continuous(stream, format='bgr', use_video_port=True):
				if not self.running:
					break
				image = frame.array
				for subscriber in self.stream_subscribers:
					k = subscriber(image)
					if k == -1:
						self.running = False
				stream.truncate(0)
				current_time = time.time()
				if self.debug:
					print(1/(current_time-self.last_frame))
				self.last_frame = current_time
		self.camera.close()

	def start_stream_capture(self):
		self.last_frame = time.time()
		t = threading.Thread(target=self._start_stream_capture)
		t.setDaemon(False)
		t.start()

	def stop_stream_capture(self):
		self.running = False

	def subscribe_stream(self, callback):
		self.stream_subscribers.append(callback)

	def transform_coordinates_zoom(self, coordinates, old_zoom, new_zoom):
		d_y = coordinates[0] - self.camera.resolution.height/2
		d_x = coordinates[1] - self.camera.resolution.width/2

		d_y *= new_zoom / old_zoom
		d_x *= new_zoom / old_zoom

		new_y = self.camera.resolution.height/2 + d_y
		new_x = self.camera.resolution.width/2 + d_x

		return (int(new_y), int(new_x))

	def transform_coordinates_resolution(self, coordinates, old_resolution=None, new_resolution=None):
		if new_resolution is None:
			new_resolution = self.camera.resolution

		if old_resolution is None:
			old_resolution = self.camera.resolution

		new_y = coordinates[0] / old_resolution[0] * new_resolution[0]
		new_x = coordinates[1] / old_resolution[1] * new_resolution[1]

		return (int(new_y), int(new_x))
